SELECT A.COD_EMPR AS EMPRESA,
       INITCAP(B.ABV_EMPR) AS UNIDADE,
       SUM(A.COL_AREA) AS AREA_COLHIDA,
       MIN(A.COL_DATA) AS INICIO_COLHEITA,
       MAX(A.COL_DATA) AS FIM_COLHEITA,
       C.PLA_AREA AS AREA_PLANTADA,
       NVL(D.PESO, 0) AS PESO_COLHIDO,
       D.QTD AS QTD_FARDOS,
       D.QTD_BOBINA AS QTD_BOBINAS,
       D.QTD_FARDAO AS QTD_FARDOES
  FROM GATEC_SAF.GA_SAF_COLHEITA A
  JOIN GATEC_SAF.GA_EMPR B
    ON A.COD_EMPR = B.COD_EMPR
  JOIN (SELECT A.COD_EMPR, A.COD_SAFRA, SUM(A.PLA_AREA) PLA_AREA
          FROM GATEC_SAF.GA_SAF_PLANTIO A
         WHERE A.COD_CULTURA = '06'
         GROUP BY A.COD_EMPR, A.COD_SAFRA) C
    ON A.COD_EMPR = C.COD_EMPR
   AND A.COD_SAFRA = C.COD_SAFRA
  LEFT JOIN (SELECT A.COD_EMPR,
                    A.COD_SAFRA,
                    SUM(NVL(A.FAR_PESO_BAL,0)) PESO,
                    SUM(CASE
                          WHEN NVL(A.FAR_PESO_BAL,0) < 3000 THEN
                           1
                          ELSE
                           0
                        END) QTD_BOBINA,
                    SUM(CASE
                          WHEN NVL(A.FAR_PESO_BAL,0) > 3000 THEN
                           1
                          ELSE
                           0
                        END) QTD_FARDAO,
                    COUNT(*) QTD
               FROM GATEC_SAF.GA_ALG_FARDAO A
              WHERE A.COD_SAFRA IN
                    (SELECT COD_SAFRA
                       FROM GATEC_SAF.GA_SAF_SAFRAS
                      WHERE SAF_ANO_SAFRA = 2014)
              GROUP BY A.COD_EMPR, A.COD_SAFRA) D
    ON A.COD_EMPR = D.COD_EMPR
   AND A.COD_SAFRA = D.COD_SAFRA
 WHERE A.ID_DIVI4 IN
       (SELECT ID_DIVI4
          FROM GATEC_SAF.GA_SAF_PLANTIO
         WHERE A.COD_SAFRA IN (SELECT COD_SAFRA
                                 FROM GATEC_SAF.GA_SAF_SAFRAS
                                WHERE SAF_ANO_SAFRA = 2014)
           AND COD_CULTURA = '06')
 GROUP BY A.COD_EMPR,
          INITCAP(B.ABV_EMPR),
          C.PLA_AREA,
          D.PESO,
          D.QTD,
          D.QTD_BOBINA,
          D.QTD_FARDAO
 ORDER BY 1
