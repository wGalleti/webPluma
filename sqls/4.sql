select initcap(trim(b.abv_empr)),
       decode(
         trunc(nvl(substr(a.cod_padrao,1,1),10)),
         1, '11 At� 17',
         2, '21 At� 27',
         3, '31 At� 37',
         4, '41 At� 47',
         5, '51 At� 57',
         6, '61 At� 67',
         7, '71 At� 77',
         8, '81 At� 87',
         9, 'Ap',
         'Sem Classifica��o'
       ) tippre,
       count(*) qtd_fardos,
       round(sum(a.plu_peso) / 1000,2) peso_ton,
       to_char(wm_concat(distinct a.cod_padrao)) tipos
  from gatec_saf.ga_alg_pluma a
       join gatec_saf.ga_empr b on a.cod_empr = b.cod_empr
 where a.cod_safra = '2013/14-1'
 group by initcap(trim(b.abv_empr)), substr(a.cod_padrao,1,1)
 order by 1, 2
