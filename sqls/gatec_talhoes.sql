create or replace view gatec_talhoes as
select tsf.cod_empr             as "IDEmpresa",
       tsf.cod_safra            as "IDSafra",
       tsf.id_divi4             as "IDTalhaoSafra",
       tsf.id_divi3             as "IDTalhao",
       initcap(tal.dsc_divi3)   as "NomeTalhao",
       tsf.cod_cultura          as "IDCultura",
       initcap(cul.dsc_cultura) as "NomeCultura",
       vd4.cod_varie            as "IDVariedade",
       initcap(vrd.dsc_varie)   as "NomeVariedade",
       tsf.dv4_area             as "AreaTalhao",
       tsf.dv4_status           as "IDStatus",
       decode(
         tsf.dv4_status,
         0, 'Reservado',
         1, 'Em Plantio',
         2, 'Em Crescimento',
         3, 'Previsto p/ Colheita',
         5, 'Liberado p/ Colheita',
         6, 'Em processo de Colheita',
         7, 'Encerrado'
       )                         as "StatusTalhao",
       tsf.dv4_plant_ini         as "DataInicioPlantio",
       tsf.dv4_plant_fim         as "DataTerminoPlantio",
       tsf.dv4_colhe_ini         as "DataInicioColheita",
       tsf.dv4_colhe_fim         as "DataTerminoColheita"
  from gatec_saf.ga_saf_divi4 tsf
       join gatec_saf.ga_saf_divi3 tal   on tsf.cod_empr = tal.cod_empr
                                        and tsf.id_divi3 = tal.id_divi3
       join gatec_saf.ga_saf_divi2 div   on tal.cod_empr = div.cod_empr
                                        and tal.cod_divi2 = div.cod_divi2
       join gatec_saf.ga_saf_divi1 blo   on tal.cod_empr  = blo.cod_empr
                                        and tal.cod_divi1 = blo.cod_divi1
       join gatec_saf.ga_saf_cultura cul on tsf.cod_cultura = cul.cod_cultura
       join gatec_saf.ga_saf_varie_divi4 vd4 on tsf.id_divi4 = vd4.id_divi4
       join gatec_saf.ga_saf_variedade vrd on vd4.cod_varie = vrd.cod_varie
 where tsf.cod_safra <> 'ISENTA'
 order by 2, 1;
