'use strict';

/**
 * @ngdoc service
 * @name webPlumaApp.sessao
 * @description
 * # sessao
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp')
  .factory('sessao', function ($route) {

    var sessao = {
      safra : "2014"
    }

    return sessao;
  });
