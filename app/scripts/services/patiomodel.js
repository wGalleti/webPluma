'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.patioModel
 * @description
 * # patioModel
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('patioModel', function(patioApi) {
    return {
        Patio : function() {
            var promise = patioApi.getPatio();

			return promise.then(function(jsonPatio){
				return jsonPatio.data;    
            });
            
        }
    };
});