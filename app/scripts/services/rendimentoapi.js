'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.rendimentoApi
 * @description
 * # rendimentoApi
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('rendimentoApi', function(sessao, $http) {

    return {
        getRendimentoVariedade: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/rendimento', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },
		getRendimentoVariedadeProdutora: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/rendimentoProdutora', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },
        getRendimentoVariedadeBeneficiadora: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/rendimentoBeneficiadora', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },
        getRendimentoTipo: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/rendimentoTipo', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },        
    };
});