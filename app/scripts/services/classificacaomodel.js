'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.classificacaoModel
 * @description
 * # classificacaoModel
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('classificacaoModel', function(classificacaoApi) {
    var jsonClassificacaoVisualGeral = [];



    return {
        VariedadesLotes : function(){
            var promise = classificacaoApi.getVariedadesLotes();

            return promise.then(function(rest){
                return rest.data;
            });
        },

        ClassificacaoVisual: function(type) {
            type = typeof(type) == 'undefined' ? 'geral' : type;

            if (type == 'geral') {
                var promise = classificacaoApi.getClassificacaoVisual();
                
                return promise.then(function(rest) {

                    return jsonClassificacaoVisualGeral = rest.data;
                });
            }
            else if (type == 'empresa')
            {
                var promise = classificacaoApi.getClassificacaoVisualEmpresa();

                return promise.then(function(rest) {
                    var json = rest.data;

                    var empresas = wPivot.getDistinct(json, 'UNIDADE');
                    var tipos = wPivot.getDistinct(json, 'COD_PADRAO');
                    var join = wPivot.joinElements(tipos, empresas);
                    var result = wPivot.fillElements(join, json, 'QTD_FARDOS', 'UNIDADE', 'COD_PADRAO');
                    var finish = [];

                    for(var key in result){

                        finish.push({
                            TIPO : key,
                            VALUE : []
                        });

                        if (Array.isArray(result[key])) {

                            for(var key2 in result[key])
                            {
                                for(var auxKey in finish)
                                {
                                    if (finish[auxKey].TIPO == key) {
                                        finish[auxKey]['VALUE'].push({
                                            QTD_FARDOS : result[key][key2]
                                        });
                                    } 
                                }
                            }
                        }
                        else
                        {
                            finish.push({TIPO: key, value : result[key]});
                        }
                        
                    }
                    return {header : empresas, data : finish};    
                });
            }
            else if (type == 'grafico')
            {
                var promise = classificacaoApi.getClassificacaoVisualEmpresa();

                return promise.then(function(rest) {
                    var json = rest.data;

                    var empresas = wPivot.getDistinct(json, 'UNIDADE');
                    var tipos = wPivot.getDistinct(json, 'COD_PADRAO');
                    var join = wPivot.joinElements(tipos, empresas);
                    var result = wPivot.fillElements(join, json, 'QTD_FARDOS', 'UNIDADE', 'COD_PADRAO');
                    
                    return {header : empresas, data : result};    
                });
            }
        },
        ClassificacaoHvi : function(){
            var promise = classificacaoApi.getClassificacaoHvi();

            return promise.then(function(rest){
                return rest.data;
            });
        }
    };
});