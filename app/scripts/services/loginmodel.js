'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.loginModel
 * @description
 * # loginModel
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('loginModel', function($window, $location, $http, md5, $rootScope, toaster, loginApi) {
    var userSession = {};
    return {
        Login: function(user) {
            var oAuth = {};
            var promise = loginApi.getUserInfo(user);
            
            return promise.then (function(data) {

                oAuth = data.data;
                if (oAuth) {
                    if (md5.createHash(user.pass) == oAuth.USERPASSWORD) {
                        $window.localStorage['UserID']   = oAuth.IDUSER;
                        $window.localStorage['UserName'] = oAuth.USERNAME;
                        $window.localStorage['UserMail'] = oAuth.USEREMAIL;
                        $window.localStorage['Token'] = md5.createHash(oAuth.UserMail + new Date());
                        
                        $rootScope.userLogged = true;
                        $location.path('/main');
                    } else {
                        $location.path('/login');
                        toaster.pop({
                            type: 'error',
                            title: 'Erro de Autenticação',
                            body: 'Senha incorreta!',
                            showCloseButton: true
                        });
                    }
                } else {
                    toaster.pop({
                        type: 'error',
                        title: 'Erro de Autenticação',
                        body: 'Usuário não encontrado',
                        showCloseButton: true
                    });
                    $location.path('/login');
                }
                return userSession;                
            }) ;
            
        },
        Logout: function() {
            $window.localStorage.removeItem('UserID');
            $window.localStorage.removeItem('UserName');
            $window.localStorage.removeItem('UserMail');
            $window.localStorage.removeItem('Token');
            $rootScope.userLogged = false;
            return true;
        },
        CheckAccess: function() {
            userSession = {
                IDUser: $window.localStorage['UserID'],
                UserName: $window.localStorage['UserName'],
                UserMail: $window.localStorage['UserMail'],
                Token: $window.localStorage['Token']
            }
            if (userSession.Token) {
                $window.localStorage['LoggedIn'] = true;
            };
            return userSession;
        }
    };
});