'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.colheitaApi
 * @description
 * # colheitaApi
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('colheitaApi', function($http, sessao) {
    return {
        getColheita: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/colheita', {
                params: {
                    anosafra: sessao.safra
                }
            })
        }
    };
});