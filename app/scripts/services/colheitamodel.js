'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.colheitaModel
 * @description
 * # colheitaModel
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('colheitaModel', function(colheitaApi) {

    
    return {
        Colheita: function(type) {
            var promise = colheitaApi.getColheita();

            return promise.then(function(rest){
                
                var jsonColheita = rest.data;

                type = typeof(type) == 'undefined' ? 'normal' : type;

                switch(type){
                    case 'normal' :
                        return jsonColheita;
                        break;
                    case 'percentualVSmedia':
                        var aux = [];
                        for(var key in jsonColheita){
                            var per = (jsonColheita[key].AREA_COLHIDA / jsonColheita[key].AREA_PLANTADA) * 100;
                            var med = (jsonColheita[key].PESO_COLHIDO / 15) / jsonColheita[key].AREA_COLHIDA;

                            aux.push({
                                "unidade" : jsonColheita[key].UNIDADE,
                                "percentual" : parseFloat(per.toFixed(2)),
                                "media" : parseFloat(med.toFixed(2))
                            });
                        }
                        return aux;
                        break;

                    case 'percentualPie':
                        var aux = [];
                        for(var key in jsonColheita){
                            aux.push({
                                name: jsonColheita[key].UNIDADE,
                                y: jsonColheita[key].AREA_COLHIDA,
                                sliced : false,
                                selected :false
                            });
                        }
                        return aux;
                        break;
                }
            });
        }    
    }
    


});