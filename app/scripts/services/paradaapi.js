'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.paradaApi
 * @description
 * # paradaApi
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('paradaApi', function(sessao, $http) {
    return {
        getParadasResumo: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/paradaResumo', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },
        getParadasHoje: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/paradaHoje', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },
        getParadasSemana: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/paradaSemana', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },  
        getParadasDetalhes: function(empresa, dia) {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/paradaDetalhe', {
                params: {
                    anosafra: sessao.safra,
                    empresa : empresa,
                    dia : dia
                }
            });
        },              
    };
});