'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.fardaoApi
 * @description
 * # fardaoApi
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('fardaoApi', function(sessao, $http) {

    return {
        getFardaoProdutora: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/fardaoProdutora', {
                params: {
                    anosafra: sessao.safra
                }
            })
        },
        getFardaoBeneficiadora: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/fardaoBeneficiadora', {
                params: {
                    anosafra: sessao.safra
                }
            })
        },     
        getStatusGeral: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/fardoStatusGeral', {
                params: {
                    anosafra: sessao.safra
                }
            })
        },   
        getPercentualGeral: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/percentualFardao', {
                params: {
                    anosafra: sessao.safra
                }
            })
        },          
    };
});