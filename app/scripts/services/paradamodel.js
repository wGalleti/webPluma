'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.paradaModel
 * @description
 * # paradaModel
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('paradaModel', function(paradaApi) {
    
    var jsonData = [];

    return {
        ParadaResumo: function() {

            var promise = paradaApi.getParadasResumo();

            return promise.then(function(rest){
                return rest.data;
            });
        },
        ParadaHoje : function(grafico){
        	grafico = typeof(grafico)  == 'undefined' ? false : grafico;

        	var promise = paradaApi.getParadasHoje();

        	if (grafico) 
        	{
        		return promise.then(function(rest){
        			var dados = rest.data;
        			var result = [];

        			for(var key in dados)
        			{
        				result.push({
        					"EMPRESA" : dados[key].EMPRESA,
        					"HORAS" : dados[key].DIAS * 24 + dados[key].HORAS + dados[key].MINUTOS / 60
        				})
        			}

        			return result;

        		});
        	}
        	else
        	{
	        	return promise.then(function(rest){
	        		return rest.data;
	        	});	
        	}
        	
        },
        TotalParadas : function(){
        	var promise = paradaApi.getParadasHoje();

        	return promise.then(function(rest){

        		var dados = rest.data;
        		var total = 0;

        		for(var key in dados)
        		{
        			total += dados[key].QTD;
        		}

        		return total;

        	});
        },
        ParadaSemana : function(){
        	var promise = paradaApi.getParadasSemana();

        	return promise.then(function(rest){
        		return rest.data;
        	});
        },
        ParadaDetalhe : function(empresa, dia)
        {
        	var promise = paradaApi.getParadasDetalhes(empresa, dia);
        	return promise.then(function(rest){
        		return rest.data;
        	});
        }
    };
});