'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.loginApi
 * @description
 * # loginApi
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('loginApi', function($http) {
    return {
        getUserInfo: function(user) 
        {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/auth', {
                params :{
                    username : user.name
                }
            });
        },
        getToken : function()
        {
            return 0;
        }
    };
});