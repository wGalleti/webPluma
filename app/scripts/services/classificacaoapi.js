'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.classificacaoApi
 * @description
 * # classificacaoApi
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('classificacaoApi', function($http, $rootScope, sessao) {
    return {
        getClassificacaoVisual: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/classificacao/visual', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },
        getClassificacaoVisualEmpresa: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/classificacao/visualEmpresa', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },
        getClassificacaoHvi: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/classificacao/hvi', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },
        getVariedadesLotes: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/variedadeLote', {
                params: {
                    anosafra: sessao.safra
                }
            });
        }       
    };
});