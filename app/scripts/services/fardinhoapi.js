'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.fardinhoApi
 * @description
 * # fardinhoApi
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('fardinhoApi', function(sessao, $http) {
    return {
        getFardos: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/fardinho/lista', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },
        getFardosHoje: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/fardinho/hoje', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },
        getFardosTurno: function(date) {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/fardinho/turno', {
                params: {
                    anosafra: sessao.safra,
                    data: date
                }
            });
        },
        getFardosSemana: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/fardinho/semana', {
                params: {
                    anosafra: sessao.safra
                }
            });
        },
        getFardosHora: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/fardinho/hora', {
                params: {
                    anosafra: sessao.safra,
                    dia : new Date()
                }
            });
        },
        getFardosProcesso: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/fardinho/processo', {
                params: {
                    anosafra: sessao.safra,
                    dia : new Date()
                }
            });
        },        
    };
});