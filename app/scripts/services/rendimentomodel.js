'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.rendimentoModel
 * @description
 * # rendimentoModel
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('rendimentoModel', function(rendimentoApi) {
    
    return {
        RendimentoVariedade: function() {
            var promise = rendimentoApi.getRendimentoVariedade();

            return promise.then(function(rest){
                var json = rest.data;
                var retorno = [];

                for(var key in json)
                {
                    var Rendimento = (json[key].PESO_PLUMA / json[key].PESO_ALGODAO) * 100;

                    retorno.push({
                        variedade : json[key].VARIEDADE,
                        rendimento : parseFloat(Rendimento.toFixed(1))
                    });
                }

                return retorno;
            });
        },
        RendimentoVariedadeProdutora: function(type) {
            type = typeof(type) == 'undefined' ? 'normal' : type;

            var promise = rendimentoApi.getRendimentoVariedadeProdutora();

            return promise.then(function(rest){
                switch(type)
                {
                    case 'normal' :
                        var json = rest.data;
                        var retorno = [];

                        for(var key in json)
                        {
                            var Rendimento = (json[key].PESO_PLUMA / json[key].PESO_ALGODAO) * 100;

                            retorno.push({
                                unidade : json[key].UNIDADE,
                                variedade : json[key].VARIEDADE,
                                rendimento : parseFloat(Rendimento.toFixed(1))
                            });
                        }

                        return retorno;
                        break;
                    case 'chart' :
                        var json = rest.data;

                        var arVariedade = wPivot.getDistinct(json, 'VARIEDADE');
                        var arUnidades  = wPivot.getDistinct(json, 'UNIDADE');
                        var aux         = wPivot.joinElements(arUnidades, arVariedade);
                        var arGrafico   = wPivot.fillElements(aux, json, 'RENDIMENTO', 'VARIEDADE', 'UNIDADE');                        

                        return {header: arVariedade, data : arGrafico};
                        break;
                }
            });
        },  
        RendimentoVariedadeBeneficiadora: function(type) {
            type = typeof(type) == 'undefined' ? 'normal' : type;

            var promise = rendimentoApi.getRendimentoVariedadeBeneficiadora();

            return promise.then(function(rest){
                switch(type)
                {
                    case 'normal' :
                        var json = rest.data;
                        var retorno = [];

                        for(var key in json)
                        {
                            var Rendimento = (json[key].PESO_PLUMA / json[key].PESO_ALGODAO) * 100;

                            retorno.push({
                                unidade : json[key].UNIDADE,
                                variedade : json[key].VARIEDADE,
                                rendimento : parseFloat(Rendimento.toFixed(1))
                            });
                        }

                        return retorno;
                        break;
                    case 'chart' :
                        var json = rest.data;

                        var arVariedade = wPivot.getDistinct(json, 'VARIEDADE');
                        var arUnidades  = wPivot.getDistinct(json, 'UNIDADE');
                        var aux         = wPivot.joinElements(arUnidades, arVariedade);
                        var arGrafico   = wPivot.fillElements(aux, json, 'RENDIMENTO', 'VARIEDADE', 'UNIDADE');                        

                        return {header: arVariedade, data : arGrafico};
                        break;
                }
            });
        },

        RendimentoTipo : function(){
            var promise = rendimentoApi.getRendimentoTipo();

            return promise.then(function(rest) {

                return rest.data;
            });
        }

    };
});