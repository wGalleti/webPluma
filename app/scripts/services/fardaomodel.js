'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.fardaoModel
 * @description
 * # fardaoModel
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('fardaoModel', function(fardaoApi) {

    return {
        
        FardaoProdutora: function() {
            var promise = fardaoApi.getFardaoProdutora();
            return promise.then(function(rest){
                return rest.data;
            });
        },
        
        FardaoBeneficiadora: function() {
            var promise = fardaoApi.getFardaoBeneficiadora();
            return promise.then(function(rest){
                return rest.data;
            });
        },
        
        TotalGeral : function(){
            var promise = fardaoApi.getStatusGeral();

            return promise.then(function(rest){
                var json = rest.data

                var aux = [];
                for(var key in json){
                    aux.push({
                        name: json[key].STATUS,
                        y: json[key].FAR_QTD,
                        sliced : true,
                        selected :false
                    });
                }
                return aux;        
            });
        },

        PercentualGeral : function(){
            var promise = fardaoApi.getPercentualGeral();

            return promise.then(function(rest){
                
                var json = rest.data;

                var Percentuais = {
                    PesoRolinho : 0,
                    PesoFardao : 0,
                    QtdRolinho : 0,
                    QtdFardao : 0
                }

                var Quantidades = {
                    Fardao : 0,
                    Rolinho : 0
                }

                var Pesos = {
                    Fardao : 0,
                    Rolinho : 0
                }

                for(var key in json)
                {
                    var PesoRolinho = (json[key].PESO_ROLINHO * 100) / json[key].PESO_TOTAL;
                    var PesoFardao = (json[key].PESO_FARDAO * 100) / json[key].PESO_TOTAL;
                    var QtdRolinho = (json[key].QTD_ROLINHO * 100) / json[key].QTD_TOTAL;
                    var QtdFardao = (json[key].QTD_FARDAO * 100) / json[key].QTD_TOTAL;

                    Quantidades.Rolinho  = json[key].QTD_ROLINHO;
                    Quantidades.Fardao  = json[key].QTD_FARDAO;

                    Pesos.Fardao = parseFloat(json[key].PESO_FARDAO.toFixed(2));
                    Pesos.Rolinho = parseFloat(json[key].PESO_ROLINHO.toFixed(2));

                }

                Percentuais.PesoRolinho = parseFloat(PesoRolinho.toFixed(2));
                Percentuais.PesoFardao = parseFloat(PesoFardao.toFixed(2));
                Percentuais.QtdRolinho = parseFloat(QtdRolinho.toFixed(2));
                Percentuais.QtdFardao = parseFloat(QtdFardao.toFixed(2));

                return {'Percentuais' : Percentuais, 'Quantidades': Quantidades, 'Pesos': Pesos};
            });
        }

    };
});