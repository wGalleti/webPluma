'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.patioApi
 * @description
 * # patioApi
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('patioApi', function($http, sessao) {
    return {
        getPatio: function() {
            return $http.get('http://intranet.gruposcheffer.com/jsonServer/pluma/apiPainel/patio', {
                params: {
                    anosafra: sessao.safra
                }
            });
        }
    };
});