'use strict';
/**
 * @ngdoc service
 * @name webPlumaApp.fardinhoModel
 * @description
 * # fardinhoModel
 * Factory in the webPlumaApp.
 */
angular.module('webPlumaApp').factory('fardinhoModel', function($filter, fardinhoApi) {

    var jsonFardos = [];
    var jsonFardosSemana = [];
    var jsonFardosHora = [];
    var jsonFardosHoje = [];

    return {
        FardosProduzidos: function(type) {            
            type = typeof(type) == 'undefined' ? 'normal' : type;

            var promise = fardinhoApi.getFardos();

            return promise.then(function(rest){
                
               jsonFardos = rest.data;

                switch (type) {
                    case 'normal':
                        return jsonFardos;
                        break;
                    case 'rendimento':
                        var aux = [];
                        var rendimento = 0;
                        var auxRendimentoFinal = {
                            PESO_PLUMA : 0,
                            PESO_ALGODAO : 0
                        }

                        for(var key in jsonFardos){
                            rendimento = (jsonFardos[key].PESO_PLUMA / jsonFardos[key].PESO_ALGODAO) * 100;
                            auxRendimentoFinal.PESO_PLUMA += jsonFardos[key].PESO_PLUMA;
                            auxRendimentoFinal.PESO_ALGODAO += jsonFardos[key].PESO_ALGODAO


                            aux.push({
                                "unidade" : jsonFardos[key].UNIDADE,
                                "rendimento" : parseFloat(rendimento.toFixed(2))
                            });
                        }
                        var rendimentoFinal = 0;
                        rendimentoFinal = (auxRendimentoFinal.PESO_PLUMA / auxRendimentoFinal.PESO_ALGODAO) * 100;

                        aux.push({
                            "unidade" : 'Total',
                            "rendimento" : parseFloat(rendimentoFinal.toFixed(2))
                        });
                        
                        return aux;
                        break;
                }
            });
        },

        FardosProcesso: function() {            

            var promise = fardinhoApi.getFardosProcesso();

            return promise.then(function(rest){
                
                jsonFardos = rest.data;
                return jsonFardos;
                
            });
        },        

        FardosProduzidosSemana : function(type){
            type = typeof(type) == 'undefined' ? 'normal' : type;

            var promise = fardinhoApi.getFardosSemana();

            return promise.then(function(rest){

                jsonFardosSemana = rest.data;
                
                switch(type){
                    case 'normal' :
                        return jsonFardosSemana;
                        break;
                    case 'linechart' :
                        var arDatas    = wPivot.getDistinct(jsonFardosSemana, 'DATA');
                        var arUnidades = wPivot.getDistinct(jsonFardosSemana, 'UNIDADE');
                        var aux        = wPivot.joinElements(arUnidades, arDatas);
                        var arGraficoDia  = wPivot.fillElements(aux, jsonFardosSemana, 'QTD_FARDOS', 'DATA', 'UNIDADE');

                        for (var key in arDatas){
                            arDatas[key] = $filter('date')($filter('Date')(arDatas[key],'US'), 'dd/MM');
                        }  

                        return {headers : arDatas, data: arGraficoDia};
                        break;
                }
            });

        },

        FardosProduzidosHoje: function() {
            var promise = fardinhoApi.getFardosHoje();
            return promise.then(function (rest) {return jsonFardosHoje = rest.data});
        },


        FardosProduzidosHora : function(type){
            type = typeof(type) == 'undefined' ? 'normal' : type;

            var promise = fardinhoApi.getFardosHora();

            return promise.then(function(rest) {
                jsonFardosHora = rest.data;

                switch(type){
                    case 'normal' : 
                        return jsonFardosHora;
                        break;
                    case 'linechart' :
                        var arDatasHora    = wPivot.getDistinct(jsonFardosHora, 'DATA_HORA');
                        var arUnidadesHora = wPivot.getDistinct(jsonFardosHora, 'UNIDADE');
                        var auxHora        = wPivot.joinElements(arUnidadesHora, arDatasHora);
                        var arGraficoHora  = wPivot.fillElements(auxHora, jsonFardosHora, 'QTD_FARDOS', 'DATA_HORA', 'UNIDADE');

                        for (var key in arDatasHora){
                            arDatasHora[key] = $filter('date')($filter('Date')(arDatasHora[key],'US'), 'dd - HH:mm');
                        }

                        return {headers : arDatasHora, data : arGraficoHora};
                        break;    
                }            

            });
        },

        FardosTurno : function(date)
        {
            var promise = fardinhoApi.getFardosTurno(date);

            return promise.then(function(rest){
                return rest.data;
            });
        }
    };
});