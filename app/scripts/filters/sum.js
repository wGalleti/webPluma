'use strict';
/**
 * @ngdoc filter
 * @name webPlumaApp.filter:Sum
 * @function
 * @description
 * # Sum
 * Filter in the webPlumaApp.
 */
angular.module('webPlumaApp').filter('Sum', function() {
    return function(data, key) 
    {
        if (typeof (data) === 'undefined' && typeof (key) === 'undefined') 
        {
            return 0;
        }

        var sum = 0;
        
        for (var pos in data) 
        {
            sum = sum + data[pos][key];
        }
        
        return sum;
    };
});