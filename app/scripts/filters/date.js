'use strict';
/**
 * @ngdoc filter
 * @name webPlumaApp.filter:Date
 * @function
 * @description
 * # Date
 * Filter in the webPlumaApp.
 */
angular.module('webPlumaApp').filter('Date', function() {
    return function(input, key) {

        if (!input) {return};

    	if (typeof(key) == 'undefined') { key = 'US'}

    	if (key == 'US') {
	        var p = input.split(/[- :]/);
	        p[3] = !p[3] ? 0 : p[3];
    		p[4] = !p[4] ? 0 : p[4];
    		p[5] = !p[5] ? 0 : p[5];
	        /* new Date(year, month [, day, hour, minute, second, millisecond]); */
	        return new Date(p[0], p[1] - 1, p[2], p[3], p[4], p[5]);
    	}
    	else
    	{
    		var p = input.split(/[- :]/);
    		p[3] = !p[3] ? 0 : p[3];
    		p[4] = !p[4] ? 0 : p[4];
    		p[5] = !p[5] ? 0 : p[5];
	        /* new Date(year, month [, day, hour, minute, second, millisecond]); */
	        return new Date(p[2], p[1] - 1, p[0], p[3], p[4], p[5]);
    	}
    };
});