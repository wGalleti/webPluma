'use strict';
/**
 * @ngdoc filter
 * @name webPlumaApp.filter:Avg
 * @function
 * @description
 * # Avg
 * Filter in the webPlumaApp.
 */
angular.module('webPlumaApp').filter('Avg', function() {
    return function(data, key) 
    {
        if (typeof(data) === 'undefined' && typeof(key) === 'undefined') {
            return 0;
        }
        var sum = 0;
        var cnt = 0;
        for (var i = 0; i < data.length; i++) 
        {
        	cnt++;
            sum = sum + data[i][key];
        }
        return sum / cnt;
    };
});