'use strict';
/**
 * @ngdoc function
 * @name webPlumaApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the webPlumaApp
 */
angular.module('webPlumaApp').controller('HomeCtrl', function($rootScope, $scope, $filter, fardinhoModel, colheitaModel) {
    // Retorna menu ativo
    $rootScope.navActive = 'dashboard';

    // Dados de Produção
    var Dados = {
        FardosProduzidos : fardinhoModel.FardosProduzidos(),
        Rendimentos : fardinhoModel.FardosProduzidos('rendimento'),
        FardosProduzidosHoje : fardinhoModel.FardosProduzidosHoje(),
        FardosProduzidosHora : fardinhoModel.FardosProduzidosHora('linechart'),
        FardosProduzidosSemana  : fardinhoModel.FardosProduzidosSemana('linechart'),
        AreaColhida : colheitaModel.Colheita()
    };

    // Data to view
    var d = {
        FardosProduzidos : {Ready : false, Data : []}, //fardinhoModel.FardosProduzidos(),
        Rendimentos : {Ready : false, Data : []}, //fardinhoModel.FardosProduzidos(),fardinhoModel.FardosProduzidos('rendimento'),
        FardosProduzidosHoje : {Ready : false, Data : []}, //fardinhoModel.FardosProduzidos(),fardinhoModel.FardosProduzidosHoje(),
        FardosProduzidosHora : {Ready : false, Data : []}, //fardinhoModel.FardosProduzidos(),fardinhoModel.FardosProduzidosHora('linechart'),
        FardosProduzidosSemana  : {Ready : false, Data : []}, //fardinhoModel.FardosProduzidos(),fardinhoModel.FardosProduzidosDia('linechart'),
        AreaColhida : {Ready : false, Data : []}, //fardinhoModel.FardosProduzidos(),colheitaModel.Colheita()
    }

    // Chart to view
    var g = {
        ProducaoSemana : {
            options: {
                chart: {
                    type: 'line'
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
            },
            series: [],
            xAxis: {
                categories: []
            },            
            yAxis :{
                title :{
                    text : null
                }
            },
            title: {
                text: ''
            },
            loading: true
        },
        ProducaoHora : {
             options: {
                chart: {
                    type: 'line'
                }, 
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },            
            title: {
                text: 'Produção por Hora'
            },
            loading: true       
        },
        ProducaoHoje : {
            options: {
                chart: {
                    type: 'bar'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },            
            title: {
                text: ''
            },
            loading: true
        }
    };    

    Dados.AreaColhida.then(function(data) {
        d.AreaColhida.Data = data;
        d.AreaColhida.Ready = true;

    });
    Dados.FardosProduzidos.then(function(data) {
        d.FardosProduzidos.Ready = true;
        d.FardosProduzidos.Data = data
    });
    Dados.Rendimentos.then(function(data) {
        d.Rendimentos.Data = data;
        d.Rendimentos.Ready = true;
    });
    Dados.FardosProduzidosSemana.then(function(data) {d.FardosProduzidosSemana = data});
    Dados.FardosProduzidosHoje.then(function(data) {

        d.FardosProduzidosHoje = data;
        // Gráfico de Produção do dia
        g.ProducaoHoje.xAxis.categories = ["Fardos Hoje"];
        for (var key in d.FardosProduzidosHoje)
        {
            g.ProducaoHoje.series.push({
                type : "bar",
                name : d.FardosProduzidosHoje[key].UNIDADE,
                data : [d.FardosProduzidosHoje[key].QTD_FARDOS]
            });
        }    
        g.ProducaoHoje.loading = false; 

    });

    Dados.FardosProduzidosHora.then(function(data) {

        d.FardosProduzidosHora = data;
        
        // Gráfico de Produção por hora
        g.ProducaoHora.xAxis.categories = d.FardosProduzidosHora.headers;
        for (var key in d.FardosProduzidosHora.data)
        {
            g.ProducaoHora.series.push({
                type : "line",
                name : key,
                data : d.FardosProduzidosHora.data[key]
            });
        }
        g.ProducaoHora.loading = false;
    });       

    // Enviar para o Scope
    $scope.g = g;
    $scope.d = d;

});