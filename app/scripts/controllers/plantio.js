'use strict';
/**
 * @ngdoc function
 * @name webPlumaApp.controller:PlantioCtrl
 * @description
 * # PlantioCtrl
 * Controller of the webPlumaApp
 */
angular.module('webPlumaApp').controller('PlantioCtrl', function($rootScope, $scope) {
	$rootScope.navActive = 'produtividade';

	$scope.AreaPlantada = [
		{"unidade" : "Rafaela", "area" : 6185, "inicio": "", "termino" : ""},
		{"unidade" : "Sperafico", "area" : 7401, "inicio": "", "termino" : ""},
		{"unidade" : "Carajás", "area" : 2679, "inicio": "", "termino" : ""},
		{"unidade" : "Três Lagoas", "area" : 4034, "inicio": "", "termino" : ""},
		{"unidade" : "Luar", "area" : 270, "inicio": "", "termino" : ""},
		{"unidade" : "Simoneti", "area" : 1470, "inicio": "", "termino" : ""},
		{"unidade" : "São Miguel", "area" : 2344, "inicio": "", "termino" : ""},
		{"unidade" : "Sanga Funda", "area" : 2323, "inicio": "", "termino" : ""}
	];

	$scope.DetalhePlantio = 
	[
		{
		    "safra": "2014/15",
		    "unidade": "Rafaela",
		    "area": 699.27,
		    "inicio_plantio": "28/12/2014",
		    "fim_plantio": "03/01/2015",
		    "variedades": "Fm 944 Gl,Fm 975 Ws,Fm 982 Gl,Tmg 81 Ws"
		}, {
		    "safra": "2014/15",
		    "unidade": "Tres Lagoas",
		    "area": 2015.058,
		    "inicio_plantio": "27/12/2014",
		    "fim_plantio": "28/02/2015",
		    "variedades": "Fm 940 Glt,Fm 980 Glt,Fm 982 Gl"
		}, {
		    "safra": "2014/15",
		    "unidade": "São Miguel",
		    "area": 827.992,
		    "inicio_plantio": "04/01/2014",
		    "fim_plantio": "09/01/2015",
		    "variedades": "Fm 944 Gl,Fm 980 Glt,Fm 982 Gl,Tmg 81 Ws"
		}, {
		    "safra": "2014/15-1",
		    "unidade": "Rafaela",
		    "area": 5485.731,
		    "inicio_plantio": "05/01/2015",
		    "fim_plantio": "13/02/2015",
		    "variedades": "Experimento,Fm 913 Glt,Fm 940 Glt,Fm 944 Gl,Fm 975 Ws,Fm 980 Glt,Fm 982 Gl,Tmg 42 Ws"
		}, {
		    "safra": "2014/15-1",
		    "unidade": "Sperafico",
		    "area": 7401.4,
		    "inicio_plantio": "11/01/2014",
		    "fim_plantio": "08/02/2015",
		    "variedades": "Dp555bgrr,Experimento,Fm 913 Glt,Fm 940 Glt,Fm 944 Gl,Fm 975 Ws,Fm 980 Glt,Fm 982 Gl,Tmg 41 Ws,Tmg 42 Ws,Tmg 43 Ws,Tmg 45 B2rf,Tmg 46 B2rf,Tmg 81 Ws,Tmg 82 Ws"
		}, {
		    "safra": "2014/15-1",
		    "unidade": "Carajás",
		    "area": 2679.79,
		    "inicio_plantio": "05/01/2015",
		    "fim_plantio": "30/01/2015",
		    "variedades": "Dp555bgrr,Fm 940 Glt,Fm 944 Gl,Fm 980 Glt,Fm 982 Gl"
		}, {
		    "safra": "2014/15-1",
		    "unidade": "Tres Lagoas",
		    "area": 2019.12,
		    "inicio_plantio": "12/01/2015",
		    "fim_plantio": "28/02/2015",
		    "variedades": "Dp555bgrr,Experimento,Fm 913 Glt,Fm 940 Glt,Fm 944 Gl,Fm 982 Gl"
		}, {
		    "safra": "2014/15-1",
		    "unidade": "Luar",
		    "area": 270.27,
		    "inicio_plantio": "29/01/2015",
		    "fim_plantio": "02/02/2015",
		    "variedades": "Fm 913 Glt,Fm 940 Glt,Fm 944 Gl,Fm 980 Glt,Fm 982 Gl"
		}, {
		    "safra": "2014/15-1",
		    "unidade": "Simoneti",
		    "area": 1470.45,
		    "inicio_plantio": "30/01/2015",
		    "fim_plantio": "12/02/2015",
		    "variedades": "Fm 913 Glt,Fm 944 Gl"
		}, {
		    "safra": "2014/15-1",
		    "unidade": "São Miguel",
		    "area": 1516.32,
		    "inicio_plantio": "15/01/2015",
		    "fim_plantio": "02/02/2015",
		    "variedades": "Fm 940 Glt,Fm 944 Gl,Fm 975 Ws,Fm 980 Glt,Tmg 42 Ws,Tmg 81 Ws"
		}, {
		    "safra": "2014/15-1",
		    "unidade": "Sanga Funda",
		    "area": 2323.96,
		    "inicio_plantio": "03/01/2015",
		    "fim_plantio": "06/02/2015",
		    "variedades": "Dp555bgrr,Fm 940 Glt,Fm 944 Gl,Fm 975 Ws,Fm 980 Glt,Tmg 42 Ws"
		}, 
	];

	$scope.VariedadePlantada = [
		{"variedade" : "DP555BGRR", "area" : 863 },
		{"variedade" : "EXPERIMENTO", "area" : 109 },
		{"variedade" : "FM 913 GLT", "area" : 2349 },
		{"variedade" : "FM 940 GLT", "area" : 7913 },
		{"variedade" : "FM 944 GL", "area" : 58825 },
		{"variedade" : "FM 975 WS", "area" : 2154 },
		{"variedade" : "FM 980 GLT", "area" : 3139 },
		{"variedade" : "FM 982 GL", "area" : 2104 },
		{"variedade" : "TMG 41 WS", "area" : 144 },
		{"variedade" : "TMG 42 WS", "area" : 1116 },
		{"variedade" : "TMG 43 WS", "area" : 140 },
		{"variedade" : "TMG 45 B2RF", "area" : 175 },
		{"variedade" : "TMG 46 B2RF", "area" : 20 },
		{"variedade" : "TMG 81 WS", "area" : 528 },
		{"variedade" : "TMG 82 WS", "area" : 67 }
	];
});