'use strict';

/**
 * @ngdoc function
 * @name webPlumaApp.controller:ModulosCtrl
 * @description
 * # ModulosCtrl
 * Controller of the webPlumaApp
 */
angular.module('webPlumaApp')
  .controller('ModulosCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
