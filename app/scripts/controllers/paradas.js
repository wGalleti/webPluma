'use strict';
/**
 * @ngdoc function
 * @name webPlumaApp.controller:ParadasCtrl
 * @description
 * # ParadasCtrl
 * Controller of the webPlumaApp
 */
angular.module('webPlumaApp').controller('ParadasCtrl', function($scope, $rootScope, paradaModel, $filter) {
    // Retorna menu ativo
    $rootScope.navActive = 'paradas';

    var Data = {
    	ParadaResumo : paradaModel.ParadaResumo(),
        ParadaHoje : paradaModel.ParadaHoje(),
        GraficoHoje : paradaModel.ParadaHoje(true),
        ParadaSemana : paradaModel.ParadaSemana()
    }

    var d = {
    	ParadaResumo : {
    		Ready : false,
    		Data : []
    	},
        ParadaHoje :{
            Ready : false,
            Data : []
        },
        ParadaSemana : {
            Ready : false,
            Data : []
        },
        DetalheParada :{
            Ready : false,
            Data : []
        }
    }

    var g = {
        ParadasHoje : {
            options: {
                chart: {
                    type: 'column'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },            
            title: {
                text: ''
            },
            loading: true
        }
    }

    var f = {
        detalheParada : function(obj, select)
        {
            $scope.paradaSelected = select;
            d.DetalheParada.Ready = true;
            var filter = {
                empresa : obj.COD_EMPR,
                data : $filter('date')($filter('Date')(obj.DATA), 'yyyy-MM-dd')
            }

            paradaModel.ParadaDetalhe(filter.empresa, filter.data).then(function(data){
                d.DetalheParada.Data = {
                    Header : obj,
                    Dados : data
                }
            });

        }
    }


    Data.ParadaResumo.then(function(data){
    	d.ParadaResumo.Ready = true;
    	d.ParadaResumo.Data = data;
    });

    Data.ParadaHoje.then(function(data){
        d.ParadaHoje.Ready = true;
        d.ParadaHoje.Data = data;
    });

    Data.GraficoHoje.then(function(data){
        for(var key in data)
        {
            g.ParadasHoje.series.push({
                name : data[key].EMPRESA,    
                type : 'column',
                data : [data[key].HORAS]
            });
        }

        g.ParadasHoje.loading = false;
        
    });

    Data.ParadaSemana.then(function(data){
        d.ParadaSemana.Ready = true;
        d.ParadaSemana.Data = data;
    });


    $scope.d = d;
    $scope.g = g;
    $scope.f = f;

});