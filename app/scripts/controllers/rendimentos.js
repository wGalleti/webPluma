'use strict';
/**
 * @ngdoc function
 * @name webPlumaApp.controller:RendimentosCtrl
 * @description
 * # RendimentosCtrl
 * Controller of the webPlumaApp
 */
angular.module('webPlumaApp').controller('RendimentosCtrl', function($rootScope, $scope, rendimentoModel, fardinhoModel, colheitaModel) {
	// Retorna menu ativo
    $rootScope.navActive = 'rendimentos';

    var Dados = {
    	RendimentoVariedade : rendimentoModel.RendimentoVariedade(),
        RendimentoVariedadeProdutora : rendimentoModel.RendimentoVariedadeProdutora('chart'),
    	RendimentoVariedadeBeneficiadora : rendimentoModel.RendimentoVariedadeBeneficiadora('chart'),
    	Rendimentos : fardinhoModel.FardosProduzidos('rendimento'),
    	AreaColhida : colheitaModel.Colheita(),

    }

    var d = {
    	RendimentoVariedade : {
    		Ready : false,
    		Data : []
    	},
    	RendimentoVariedadeProdutora :{
    		Ready : false,
    		Data : []
    	},
        RendimentoVariedadeBeneficiadora :{
            Ready : false,
            Data : []
        },        
    	Rendimentos : {
    		Ready : false, 
    		Data : []
    	},
    	AreaColhida : {
    		Ready : false, 
    		Data : []
    	},
    }

    var g = {
    	RendimentoVariedade : {
            options: {
                chart: {
                    type: 'column'
                },
                plotOptions: {
                    column: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            },
            series: [],
            xAxis: {
                categories: [],
            },
            yAxis :{
                title :{
                    text : null
                }
            },
            title: {
                text: ''
            },
            loading: true
        },
        RendimentoVariedadeProdutora : {
            options: {
                chart: {
                    type: 'column'
                },
                plotOptions: {
                    column: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },            
            title: {
                text: 'Rendimento por Variedade por unidade Produtora'
            },
            loading: true
        },
        RendimentoVariedadeBeneficiadora : {
            options: {
                chart: {
                    type: 'column'
                },
                plotOptions: {
                    column: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },            
            title: {
                text: 'Rendimento por Variedade por unidade Beneficiadora'
            },
            loading: true
        }        
    }

    Dados.RendimentoVariedade.then(function(data){
    	d.RendimentoVariedade.Ready = true;
    	d.RendimentoVariedade.Data = data;

    	g.RendimentoVariedade.xAxis.categories = ["Rendimento"];

    	for(var key in data)
    	{
    		g.RendimentoVariedade.series.push({
    			type : 'column',
    			name : data[key].variedade,
    			data : [data[key].rendimento]
    		});
    	}

    	g.RendimentoVariedade.loading = false;
    });


   	Dados.Rendimentos.then(function(data) {
        d.Rendimentos.Data = data;
        d.Rendimentos.Ready = true;
    });

    Dados.AreaColhida.then(function(data) {
        d.AreaColhida.Data = data;
        d.AreaColhida.Ready = true;

    });

    Dados.RendimentoVariedadeProdutora.then(function(data){
    	d.RendimentoVariedadeProdutora.Ready = true;
    	d.RendimentoVariedadeProdutora.Data = data;

    	g.RendimentoVariedadeProdutora.xAxis.categories = data.header;
    	for(var key in data.data)
    	{
    		g.RendimentoVariedadeProdutora.series.push({
    			type : 'column',
    			name : key,
    			data : data.data[key]
    		});
    	}
    	g.RendimentoVariedadeProdutora.loading = false;
    });  

    Dados.RendimentoVariedadeBeneficiadora.then(function(data){
        d.RendimentoVariedadeBeneficiadora.Ready = true;
        d.RendimentoVariedadeBeneficiadora.Data = data;

        g.RendimentoVariedadeBeneficiadora.xAxis.categories = data.header;
        for(var key in data.data)
        {
            g.RendimentoVariedadeBeneficiadora.series.push({
                type : 'column',
                name : key,
                data : data.data[key]
            });
        }
        g.RendimentoVariedadeBeneficiadora.loading = false;
    });      

    $scope.g = g;
    $scope.d = d;
});