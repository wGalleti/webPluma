'use strict';
/**
 * @ngdoc function
 * @name webPlumaApp.controller:BeneficiamentoCtrl
 * @description
 * # BeneficiamentoCtrl
 * Controller of the webPlumaApp
 */
angular.module('webPlumaApp').controller('BeneficiamentoCtrl', function($scope, $rootScope, $filter, fardinhoModel) {
	// Retorna menu ativo
    $rootScope.navActive = 'industria';

    // Dados de Produção
    var Dados = {
        FardosProduzidos : fardinhoModel.FardosProduzidos(),
        Rendimentos : fardinhoModel.FardosProduzidos('rendimento'),
        FardosProduzidosHoje : fardinhoModel.FardosProduzidosHoje(),
        FardosProduzidosHora : fardinhoModel.FardosProduzidosHora('linechart'),
        FardosProduzidosSemana  : fardinhoModel.FardosProduzidosSemana('linechart'),
        FardosProcesso  : fardinhoModel.FardosProcesso(),
        FardosTurno : fardinhoModel.FardosTurno()
    };

    // Data to view
    var d = {
        FardosProduzidos : {Ready : false, Data : []},
        Rendimentos : {Ready : false, Data : []},
        FardosProduzidosHoje : {Ready : false, Data : []},
        FardosProduzidosHora : {Ready : false, Data : []},
        FardosProduzidosSemana  : {Ready : false, Data : []},
        FardosProcesso : {Ready : false, Data : []},
        FardosTurno : {Ready : false, Data : []},
        dataFiltro : $filter('date')(Date.now(), 'dd/MM/yyyy'),
        MostrarFiltroData : false
    }

    var f = {
        Filtrar : function() {
            d.FardosTurno.Ready = false;
            d.FardosTurno.Data = [];
            d.MostrarFiltroData = false;
            var data = $scope.d.dataFiltro;

            var p = data.split(/[/ :]/);
            data = new Date(p[2], p[1] - 1, p[0]);
            var datastr = $filter('date')(data,'yyyy-MM-dd');


            console.log(datastr);
            var NovoTurno = fardinhoModel.FardosTurno(datastr);

            NovoTurno.then(function(data){

                d.FardosTurno.Ready = true;
                d.FardosTurno.Data = data;

                $scope.d = d;
            });
        },
        MostraFiltro : function(){
            d.MostrarFiltroData = d.MostrarFiltroData == true ? false : true;
        }
    }

    // Chart to view
    var g = {
        ProducaoSemana : {
            options: {
                chart: {
                    type: 'line'
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },            
            title: {
                text: ''
            },
            loading: true
        },
        ProducaoHora : {
             options: {
                chart: {
                    type: 'line'
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },            
            title: {
                text: 'Produção por Hora'
            },
            loading: true       
        },
        ProducaoHoje : {
            options: {
                chart: {
                    type: 'bar'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },            
            title: {
                text: ''
            },
            loading: true
        }
    };

    Dados.FardosTurno.then(function(data){
        d.FardosTurno.Ready = true;
        d.FardosTurno.Data = data;
    });

    Dados.FardosProcesso.then(function(data){
        d.FardosProcesso.Data = data;
        d.FardosProcesso.Ready = true;
    });    

    Dados.FardosProduzidos.then(function(data) {
        d.FardosProduzidos.Data = data;
        d.FardosProduzidos.Ready = true;
    });
    Dados.Rendimentos.then(function(data) {
        d.Rendimentos.Data = data;
        d.Rendimentos.Ready = true;
    });
    Dados.FardosProduzidosSemana.then(function(data) {
        d.FardosProduzidosSemana.Data = data;

        // Gráfico de Produção da Semana
        g.ProducaoSemana.xAxis.categories = d.FardosProduzidosSemana.Data.headers;
        for (var key in d.FardosProduzidosSemana.Data.data){
            g.ProducaoSemana.series.push({
                type : 'line', 
                name: key, 
                data: d.FardosProduzidosSemana.Data.data[key]
            });
        }
        g.ProducaoSemana.loading = false;
    });

    Dados.FardosProduzidosHoje.then(function(data) {

        d.FardosProduzidosHoje = data;
        // Gráfico de Produção do dia
        g.ProducaoHoje.xAxis.categories = ["Fardos Hoje"];
        for (var key in d.FardosProduzidosHoje)
        {
            g.ProducaoHoje.series.push({
                type : "bar",
                name : d.FardosProduzidosHoje[key].UNIDADE,
                data : [d.FardosProduzidosHoje[key].QTD_FARDOS]
            });
        }    
        g.ProducaoHoje.loading = false; 

    });

    Dados.FardosProduzidosHora.then(function(data) {
        
        d.FardosProduzidosHora = data;
        
        // Gráfico de Produção por hora
        g.ProducaoHora.xAxis.categories = d.FardosProduzidosHora.headers;
        for (var key in d.FardosProduzidosHora.data)
        {
            g.ProducaoHora.series.push({
                type : "line",
                name : key,
                data : d.FardosProduzidosHora.data[key]
            });
        }
        g.ProducaoHora.loading = false;
    });   

    // Enviar para o Scope
    $scope.g = g;
    $scope.d = d;
    $scope.f = f;
});