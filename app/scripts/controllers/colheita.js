'use strict';
/**
 * @ngdoc function
 * @name webPlumaApp.controller:ColheitaCtrl
 * @description
 * # ColheitaCtrl
 * Controller of the webPlumaApp
 */
angular.module('webPlumaApp').controller('ColheitaCtrl', function($rootScope, $scope, colheitaModel) {
    // Retorna menu ativo
    $rootScope.navActive = 'colheita';
    var Data = {
        AreaColhida : colheitaModel.Colheita(),
        PercentualVSMedia : colheitaModel.Colheita('percentualVSmedia'),
        PercentualUnidadePie : colheitaModel.Colheita('percentualPie')
    }

    // Data 
    var d = {
        AreaColhida: {Ready : false, Data : []},
        PercentualVSMedia: {Ready : false, Data : []},
        PercentualUnidadePie: {Ready : false, Data : []}
    }

    // Charts
    var g = {
        PercentualVSMedia: {
            options: {
                chart: {
                    type: 'column'
                }
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },
            title: {
                text: 'Comparação de % x @/HA'
            },
            loading: true
        },
        PercentualUnidade: {
            options: {
                chart: {
                    type: 'pie'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer'
                    }
                }
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },            
            title: {
                text: 'Referência ao Total'
            },
            loading: true
        }
    }


            
    Data.AreaColhida.then(function(data) {
        d.AreaColhida.Data = data;
        d.AreaColhida.Ready = true;
    });
    
    Data.PercentualVSMedia.then(function(data) {
        d.PercentualVSMedia = data;

        g.PercentualVSMedia.xAxis.categories = ["Média", "% Colhido"];
        for (var key in d.PercentualVSMedia) {
            g.PercentualVSMedia.series.push({
                type: "column",
                name: d.PercentualVSMedia[key].unidade,
                data: [d.PercentualVSMedia[key].media, d.PercentualVSMedia[key].percentual]
            });
        };
        g.PercentualVSMedia.loading = false;                
    });

    Data.PercentualUnidadePie.then(function(data) {
        d.PercentualUnidadePie = data;
        
        g.PercentualUnidade.series.push({
            type: "pie",
            name: "Comparativo",
            data: d.PercentualUnidadePie
        });
        g.PercentualUnidade.loading = false;                
    });

    $scope.g = g;
    $scope.d = d;
});