'use strict';
/**
 * @ngdoc function
 * @name webPlumaApp.controller:EvolucaoCtrl
 * @description
 * # EvolucaoCtrl
 * Controller of the webPlumaApp
 */
angular.module('webPlumaApp').controller('EvolucaoCtrl', function($scope, rendimentoModel) {
    var Dados = {
    	RendimentoTipo : rendimentoModel.RendimentoTipo()
    }

    var d = {
    	RendimentoTipo : {
    		Data : [],
    		Ready : false
    	}
    }

    Dados.RendimentoTipo.then(function(data){
    	d.RendimentoTipo.Ready = true;
    	d.RendimentoTipo.Data = data;
    });


    $scope.d = d;
});