'use strict';
/**
 * @ngdoc function
 * @name webPlumaApp.controller:ClassificacaoCtrl
 * @description
 * # ClassificacaoCtrl
 * Controller of the webPlumaApp
 */
angular.module('webPlumaApp').controller('ClassificacaoCtrl', function($scope, $rootScope, classificacaoModel) {
    // Retorna menu ativo
    $rootScope.navActive = 'industria';
    var PivotTotais = [];

    $scope.addTotal = function(value, field){
        PivotTotais = 0;
        for(var key in value){
            PivotTotais += value[key][field];
        }

        $scope.PivotTotais = PivotTotais;
    }

    var d = {
        ClassificacaoVisual : [],
        ClassificacaoVisualEmpresa : [],
        ClassificacaoHvi : [],
        VariedadesLotes : []
    }

    var g = {
        ClassificacaoVisual : {
            options: {
                chart: {
                    type: 'bubble'
                },
                plotOptions: {
                    column: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },            
            title: {
                text: ''
            },
            loading: true
        }
    }

    var Dados = {
        ClassificacaoVisual  : classificacaoModel.ClassificacaoVisual(),
        ClassificacaoVisualEmpresa : classificacaoModel.ClassificacaoVisual('empresa'),
        ClassificacaoVisualGrafico : classificacaoModel.ClassificacaoVisual('grafico'),
        ClassificacaoHvi : classificacaoModel.ClassificacaoHvi(),
        VariedadesLotes : classificacaoModel.VariedadesLotes()
    }


    Dados.ClassificacaoVisual.then(function(data){
        d.ClassificacaoVisual = data;

        $scope.d = d;
    });

    Dados.ClassificacaoVisualEmpresa.then(function(data){
        d.ClassificacaoVisualEmpresa = data;
        $scope.d = d;
    });

    Dados.ClassificacaoHvi.then(function(data){
        d.ClassificacaoHvi = data;
    });

    Dados.VariedadesLotes.then(function(data){
        d.VariedadesLotes = data;
    });

    Dados.ClassificacaoVisualGrafico.then(function(data){

        g.ClassificacaoVisual.xAxis.categories = data.header;

        for(var key in data.data){
            g.ClassificacaoVisual.series.push({
                type : 'bubble',
                name : key,
                data : data.data[key]
            });
        }
        g.ClassificacaoVisual.loading = false;

        $scope.g = g;

    });

});