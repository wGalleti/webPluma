'use strict';
/**
 * @ngdoc function
 * @name webPlumaApp.controller:PatioCtrl
 * @description
 * # PatioCtrl
 * Controller of the webPlumaApp
 */
angular.module('webPlumaApp').controller('PatioCtrl', function($scope, patioModel, fardaoModel, $rootScope) {
    $rootScope.navActive = 'industria';
    
    var Dados = {
        Patio : patioModel.Patio(),
        FardaoProdutora : fardaoModel.FardaoProdutora(),
        FardaoBeneficiadora : fardaoModel.FardaoBeneficiadora(),
        TotalGeralFardao : fardaoModel.TotalGeral(),
        PercentualFardao : fardaoModel.PercentualGeral()
    }

    var g = {
        meter : [],        
    };

    var c = {
        StatusGeral: {
            options: {
                chart: {
                    type: 'pie',
                    reflow: true,
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 10
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer'
                    }
                }
            },
            series: [],
            xAxis: {
                categories: []
            },
            yAxis :{
                title :{
                    text : null
                }
            },            
            title: {
                text: ''
            },
            loading: true
        }
    }

    var d = {
        Patio : {Ready : false, Data : []},
        FardaoProdutora : {Ready : false, Data : []},
        FardaoBeneficiadora : {Ready : false, Data : []},
        FardosGerais : {Ready : false, Data : []}
    };

    Dados.FardaoProdutora.then(function(data){
        d.FardaoProdutora.Ready = true;
        d.FardaoProdutora.Data = data;
    });


    Dados.FardaoBeneficiadora.then(function(data){
        d.FardaoBeneficiadora.Ready = true;
        d.FardaoBeneficiadora.Data = data;
    });

    Dados.TotalGeralFardao.then(function(data){
        
        c.StatusGeral.series.push({
            type: "pie",
            name: "Comparativo",
            data: data
        });
        c.StatusGeral.loading = false;    
    });

    Dados.PercentualFardao.then(function(data){
        d.FardosGerais.Ready = true;
        d.FardosGerais.Data = data;
    });

    Dados.Patio.then(function(data){
        d.Patio.Data = data;
        d.Patio.Ready = true;

        for (var key in d.Patio.Data) {            
            var valor = (d.Patio.Data[key].PESO_RECEBIDO / d.Patio.Data[key].CAPACIDADE) * 100;

            g.meter.push({
                "unidade": d.Patio.Data[key].UNIDADE,
                "grafico": {
                    options: {
                        chart: {
                            type: 'solidgauge'
                        },
                        pane: {
                            center: ['50%', '70%'],
                            size: '100%',
                            startAngle: -90,
                            endAngle: 90,
                            background: {
                                innerRadius: '60%',
                                outerRadius: '100%',
                                shape: 'arc'
                            }
                        }
                    },
                    series: [{
                        name: d.Patio.Data[key].UNIDADE,
                        data: [parseFloat(valor.toFixed(2))]
                    }],
                    xAxis: {
                        categories: []
                    },
                    yAxis: {
                        stops: [
                            [0.1, '#55BF3B'],
                            [0.7, '#DDDF0D'],
                            [0.9, '#DF5353']
                        ],
                        min: 0,
                        max: 100
                    },
                    title: {
                        text: d.Patio.Data[key].UNIDADE
                    },
                    loading: false
                }
            });
        };        
    });
    
    // retorna de qualquer jeito pra view
    $scope.d = d;
    $scope.g = g;     
    $scope.c = c;   
});