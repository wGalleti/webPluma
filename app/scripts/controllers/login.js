'use strict';
/**
 * @ngdoc function
 * @name webPlumaApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the webPlumaApp
 */
angular.module('webPlumaApp').controller('LoginCtrl', function($scope, loginModel, $location) {
    $scope.Logar = function() {
        var logininfo = loginModel.Login($scope.user);
        if (logininfo.token) {
            $location.path('/main');
        };
    }
    $scope.Logout = function() {
        loginModel.Logout();
        $location.path('/login');
    }
});