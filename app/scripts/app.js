'use strict';
/**
 * @ngdoc overview
 * @name webPlumaApp
 * @description
 * # webPlumaApp
 *
 * Main module of the application.
 */
angular.module('webPlumaApp', ['ngAnimate', 'ngAria', 'ngCookies', 'ngMessages', 'ngResource', 'ngRoute', 'ngSanitize', 'ngTouch', 'highcharts-ng', 'angular-md5', 'toaster', 'ui.grid']).
config(function($routeProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
    }).when('/colheita', {
        templateUrl: 'views/colheita.html',
        controller: 'ColheitaCtrl'
    }).when('/patio', {
        templateUrl: 'views/patio.html',
        controller: 'PatioCtrl',
        controllerAs: 'patio'
    }).when('/beneficiamento', {
        templateUrl: 'views/beneficiamento.html',
        controller: 'BeneficiamentoCtrl'
    }).when('/classificacao', {
        templateUrl: 'views/classificacao.html',
        controller: 'ClassificacaoCtrl'
    }).when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
    }).when('/paradas', {
      templateUrl: 'views/paradas.html',
      controller: 'ParadasCtrl',
      controllerAs: 'paradas'
    })
    .when('/rendimentos', {
      templateUrl: 'views/rendimentos.html',
      controller: 'RendimentosCtrl',
      controllerAs: 'rendimentos'
    })
    .when('/evolucao', {
      templateUrl: 'views/evolucao.html',
      controller: 'EvolucaoCtrl',
      controllerAs: 'evolucao'
    })
    .otherwise({
        redirectTo: '/login'
    });
}).run(function($rootScope, $location, $route, loginModel, paradaModel, sessao) {

    var s = sessao;

    $rootScope.s = s;

    $rootScope.$watch('s.safra', function(){
        $route.reload();
    })
    
    paradaModel.TotalParadas().then(function(data){
        $rootScope.totalParadas = data;
    });
    
    $rootScope.$on("$routeChangeStart", function(event, next, current) {
        $rootScope.userLogged = false;
        var Check = loginModel.CheckAccess();
        if (Check.Token) {
            $rootScope.userLogged = true;
            $rootScope.IDUsuario = Check.IDUsuario;
            $rootScope.UserName = Check.UserName;
            $rootScope.UserMail = Check.UserMail;
            $rootScope.Token = Check.Token;
            if ($location.path() == '/login') {
                $location.path('/home');
            }
        } else {
            var nextUrl = next;
            if (nextUrl != '/login') {
                $location.path("/login");
            }
        }
    });
});