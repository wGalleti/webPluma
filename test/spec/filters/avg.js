'use strict';

describe('Filter: Avg', function () {

  // load the filter's module
  beforeEach(module('webPlumaApp'));

  // initialize a new instance of the filter before each test
  var Avg;
  beforeEach(inject(function ($filter) {
    Avg = $filter('Avg');
  }));

  it('should return the input prefixed with "Avg filter:"', function () {
    var text = 'angularjs';
    expect(Avg(text)).toBe('Avg filter: ' + text);
  });

});
