'use strict';

describe('Filter: Date', function () {

  // load the filter's module
  beforeEach(module('webPlumaApp'));

  // initialize a new instance of the filter before each test
  var Date;
  beforeEach(inject(function ($filter) {
    Date = $filter('Date');
  }));

  it('should return the input prefixed with "Date filter:"', function () {
    var text = 'angularjs';
    expect(Date(text)).toBe('Date filter: ' + text);
  });

});
