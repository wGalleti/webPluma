'use strict';

describe('Filter: Sum', function () {

  // load the filter's module
  beforeEach(module('webPlumaApp'));

  // initialize a new instance of the filter before each test
  var Sum;
  beforeEach(inject(function ($filter) {
    Sum = $filter('Sum');
  }));

  it('should return the input prefixed with "Sum filter:"', function () {
    var text = 'angularjs';
    expect(Sum(text)).toBe('Sum filter: ' + text);
  });

});
