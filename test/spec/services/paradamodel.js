'use strict';

describe('Service: paradaModel', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var paradaModel;
  beforeEach(inject(function (_paradaModel_) {
    paradaModel = _paradaModel_;
  }));

  it('should do something', function () {
    expect(!!paradaModel).toBe(true);
  });

});
