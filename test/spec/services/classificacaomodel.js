'use strict';

describe('Service: classificacaoModel', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var classificacaoModel;
  beforeEach(inject(function (_classificacaoModel_) {
    classificacaoModel = _classificacaoModel_;
  }));

  it('should do something', function () {
    expect(!!classificacaoModel).toBe(true);
  });

});
