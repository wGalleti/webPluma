'use strict';

describe('Service: classificacaoApi', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var classificacaoApi;
  beforeEach(inject(function (_classificacaoApi_) {
    classificacaoApi = _classificacaoApi_;
  }));

  it('should do something', function () {
    expect(!!classificacaoApi).toBe(true);
  });

});
