'use strict';

describe('Service: loginModel', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var loginModel;
  beforeEach(inject(function (_loginModel_) {
    loginModel = _loginModel_;
  }));

  it('should do something', function () {
    expect(!!loginModel).toBe(true);
  });

});
