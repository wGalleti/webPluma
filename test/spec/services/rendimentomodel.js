'use strict';

describe('Service: rendimentoModel', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var rendimentoModel;
  beforeEach(inject(function (_rendimentoModel_) {
    rendimentoModel = _rendimentoModel_;
  }));

  it('should do something', function () {
    expect(!!rendimentoModel).toBe(true);
  });

});
