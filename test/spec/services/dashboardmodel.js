'use strict';

describe('Service: dashboardModel', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var dashboardModel;
  beforeEach(inject(function (_dashboardModel_) {
    dashboardModel = _dashboardModel_;
  }));

  it('should do something', function () {
    expect(!!dashboardModel).toBe(true);
  });

});
