'use strict';

describe('Service: patioModel', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var patioModel;
  beforeEach(inject(function (_patioModel_) {
    patioModel = _patioModel_;
  }));

  it('should do something', function () {
    expect(!!patioModel).toBe(true);
  });

});
