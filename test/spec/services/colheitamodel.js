'use strict';

describe('Service: colheitaModel', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var colheitaModel;
  beforeEach(inject(function (_colheitaModel_) {
    colheitaModel = _colheitaModel_;
  }));

  it('should do something', function () {
    expect(!!colheitaModel).toBe(true);
  });

});
