'use strict';

describe('Service: rendimentoApi', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var rendimentoApi;
  beforeEach(inject(function (_rendimentoApi_) {
    rendimentoApi = _rendimentoApi_;
  }));

  it('should do something', function () {
    expect(!!rendimentoApi).toBe(true);
  });

});
