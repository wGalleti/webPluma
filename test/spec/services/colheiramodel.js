'use strict';

describe('Service: colheiraModel', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var colheiraModel;
  beforeEach(inject(function (_colheiraModel_) {
    colheiraModel = _colheiraModel_;
  }));

  it('should do something', function () {
    expect(!!colheiraModel).toBe(true);
  });

});
