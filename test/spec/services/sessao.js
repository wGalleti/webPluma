'use strict';

describe('Service: sessao', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var sessao;
  beforeEach(inject(function (_sessao_) {
    sessao = _sessao_;
  }));

  it('should do something', function () {
    expect(!!sessao).toBe(true);
  });

});
