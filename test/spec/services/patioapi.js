'use strict';

describe('Service: patioApi', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var patioApi;
  beforeEach(inject(function (_patioApi_) {
    patioApi = _patioApi_;
  }));

  it('should do something', function () {
    expect(!!patioApi).toBe(true);
  });

});
