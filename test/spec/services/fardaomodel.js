'use strict';

describe('Service: fardaoModel', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var fardaoModel;
  beforeEach(inject(function (_fardaoModel_) {
    fardaoModel = _fardaoModel_;
  }));

  it('should do something', function () {
    expect(!!fardaoModel).toBe(true);
  });

});
