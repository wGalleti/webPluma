'use strict';

describe('Service: paradaApi', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var paradaApi;
  beforeEach(inject(function (_paradaApi_) {
    paradaApi = _paradaApi_;
  }));

  it('should do something', function () {
    expect(!!paradaApi).toBe(true);
  });

});
