'use strict';

describe('Service: fardinhoApi', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var fardinhoApi;
  beforeEach(inject(function (_fardinhoApi_) {
    fardinhoApi = _fardinhoApi_;
  }));

  it('should do something', function () {
    expect(!!fardinhoApi).toBe(true);
  });

});
