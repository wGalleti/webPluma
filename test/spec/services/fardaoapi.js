'use strict';

describe('Service: fardaoApi', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var fardaoApi;
  beforeEach(inject(function (_fardaoApi_) {
    fardaoApi = _fardaoApi_;
  }));

  it('should do something', function () {
    expect(!!fardaoApi).toBe(true);
  });

});
