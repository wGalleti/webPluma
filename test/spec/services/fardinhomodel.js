'use strict';

describe('Service: fardinhoModel', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var fardinhoModel;
  beforeEach(inject(function (_fardinhoModel_) {
    fardinhoModel = _fardinhoModel_;
  }));

  it('should do something', function () {
    expect(!!fardinhoModel).toBe(true);
  });

});
