'use strict';

describe('Service: loginApi', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var loginApi;
  beforeEach(inject(function (_loginApi_) {
    loginApi = _loginApi_;
  }));

  it('should do something', function () {
    expect(!!loginApi).toBe(true);
  });

});
