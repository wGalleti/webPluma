'use strict';

describe('Service: colheitaApi', function () {

  // load the service's module
  beforeEach(module('webPlumaApp'));

  // instantiate service
  var colheitaApi;
  beforeEach(inject(function (_colheitaApi_) {
    colheitaApi = _colheitaApi_;
  }));

  it('should do something', function () {
    expect(!!colheitaApi).toBe(true);
  });

});
