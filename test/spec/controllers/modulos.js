'use strict';

describe('Controller: ModulosCtrl', function () {

  // load the controller's module
  beforeEach(module('webPlumaApp'));

  var ModulosCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModulosCtrl = $controller('ModulosCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
