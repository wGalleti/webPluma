'use strict';

describe('Controller: ClassificacaoCtrl', function () {

  // load the controller's module
  beforeEach(module('webPlumaApp'));

  var ClassificacaoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ClassificacaoCtrl = $controller('ClassificacaoCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
