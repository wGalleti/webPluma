'use strict';

describe('Controller: BeneficiamentoCtrl', function () {

  // load the controller's module
  beforeEach(module('webPlumaApp'));

  var BeneficiamentoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BeneficiamentoCtrl = $controller('BeneficiamentoCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
