'use strict';

describe('Controller: ColheitaCtrl', function () {

  // load the controller's module
  beforeEach(module('webPlumaApp'));

  var ColheitaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ColheitaCtrl = $controller('ColheitaCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
