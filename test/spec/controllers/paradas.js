'use strict';

describe('Controller: ParadasCtrl', function () {

  // load the controller's module
  beforeEach(module('webPlumaApp'));

  var ParadasCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ParadasCtrl = $controller('ParadasCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ParadasCtrl.awesomeThings.length).toBe(3);
  });
});
