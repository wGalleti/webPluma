'use strict';

describe('Controller: PatioCtrl', function () {

  // load the controller's module
  beforeEach(module('webPlumaApp'));

  var PatioCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PatioCtrl = $controller('PatioCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
