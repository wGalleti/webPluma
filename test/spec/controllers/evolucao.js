'use strict';

describe('Controller: EvolucaoCtrl', function () {

  // load the controller's module
  beforeEach(module('webPlumaApp'));

  var EvolucaoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EvolucaoCtrl = $controller('EvolucaoCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EvolucaoCtrl.awesomeThings.length).toBe(3);
  });
});
