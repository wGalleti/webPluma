'use strict';

describe('Controller: RendimentosCtrl', function () {

  // load the controller's module
  beforeEach(module('webPlumaApp'));

  var RendimentosCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RendimentosCtrl = $controller('RendimentosCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RendimentosCtrl.awesomeThings.length).toBe(3);
  });
});
